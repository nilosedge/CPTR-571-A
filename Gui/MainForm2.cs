using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace CPTR571A {
	public class MainForm2 : Form {

		private StatusBar statusbar;
		private String selectedFolder;
		
		public MainForm2() {
			Text = "Constraint Database Example";
			Size = new Size(1024, 600);
			InitializeComponents();
			CenterToScreen();
		}
		
		private void InitializeComponents() {

			Panel p = new Panel();
			p.Location = new Point(30, 120);
			p.Parent = this;

//			TextBox tb = new TextBox();
//			tb.Parent = this;
//			tb.Dock = DockStyle.Fill;
//			tb.Location = new Point(400, 400);


			
			TreeView tv = new TreeView();
			tv.Location = new Point(30, 120);
			
			TreeNode child1 = new TreeNode("Python");
			TreeNode child2 = new TreeNode("Ruby");
			TreeNode child3 = new TreeNode("Java");
			TreeNode[] array = new TreeNode[] { child1, child2, child3 };
			
			TreeNode root = new TreeNode("Languages", array);
			
			tv.Nodes.Add(root);
			tv.Dock = DockStyle.Fill;
			tv.AfterSelect += new TreeViewEventHandler(AfterSelect);
			tv.Parent = this;


			
			statusbar = new StatusBar();
			statusbar.Parent = this;



//			ToolBar toolbar = new ToolBar();
//			toolbar.Buttons.Add(new ToolBarButton());
//			toolbar.ButtonClick += new ToolBarButtonClickEventHandler(OnClicked);
//			Controls.Add(toolbar);

			
			
		}
		
		void AfterSelect(object sender, TreeViewEventArgs e) {
			statusbar.Text = e.Node.Text;
		}
		
		void OnClicked(object sender, ToolBarButtonClickEventArgs e) {
			FolderBrowserDialog dialog = new FolderBrowserDialog();
			
			if (dialog.ShowDialog(this) == DialogResult.OK) {
				selectedFolder = dialog.SelectedPath;
				statusbar.Text = selectedFolder;
			}
		}
	}
}