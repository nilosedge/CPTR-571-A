grammar Grammer;

options {
  language=CSharp2;
}

tokens {
	PLUS 	= '+' ;
	MINUS	= '-' ;
	MULT	= '*' ;
	DIV	= '/' ;


	RPAREN	= ')' ;
	LPAREN	= '(' ;
	COMMA		= ',' ;
	
	GEQ     = '>=' ;
	LEQ     = '<=' ;
	EQ      = '=' ;
	LT      = '<' ;
	GT      = '>' ;
	NEQ     = '!=' ;
}

@header {
using CPTR571A;
using System;
}

@members {
public static void Main(string[] args) {
	Console.WriteLine ("type '@' to quit...");
	string line = "";
	while (true) {
		line = Console.ReadLine ();
		if (line.Contains ("@")) {
			break;
		}
		GrammerLexer lex = new GrammerLexer (new ANTLRStringStream (line + Environment.NewLine));
		CommonTokenStream tokens = new CommonTokenStream (lex);
		GrammerParser parser = new GrammerParser (tokens);
		try {
			ClauseNode p = parser.clause();
			PrinterVisitor pr = new PrinterVisitor(Console.Out);
			XMLPrinterVisitor xpr = new XMLPrinterVisitor(Console.Out);
			p.Accept(pr);
			p.Accept(xpr);
		} catch (TreeNodeException e) {
			String hdr = parser.GetErrorHeader (e.recognitionException);
			String msg = parser.GetErrorMessage (e.recognitionException, e.tokenNames);

			for (int i = 0; i < e.recognitionException.CharPositionInLine; i++) {
				Console.Write (" ");
			}
			Console.WriteLine("^");
			Console.WriteLine("An error occured on: " + hdr + " " + msg);
		}
	}
}

public override void DisplayRecognitionError(String[] tokenNames, RecognitionException e) {
	throw new TreeNodeException(tokenNames, e);
}
}

   
clause returns[ClauseNode clause]:
   s=subject ':-' p=predicate '.' NEWLINE? { return new ClauseNode(s, p); };

subject returns[SubjectNode subject]:
	db=dbinfo { return new SubjectNode(db); }
	;

predicate returns[PredicateNode predicate] @init { predicate = new PredicateNode(); }:
	n1=node[predicate] (COMMA n2=node[predicate])*
	;

node [PredicateNode p]:
   db=dbinfo { p.addDBInfo(db); }
	| co=constraint { p.addConstraint(co); }
	;

dbinfo returns[DBInfoNode dbinfo]:
   vn=variable_name LPAREN vl=var_list RPAREN { return new DBInfoNode(vn, vl); }
	;
  
//var_list:	
//   var_or_number(',' var_or_number)*;
var_list returns[VariableListNode var_list] @init { var_list = new VariableListNode(); }:
   //variable { var_list.addVariable(variable()); } (COMMA variable { var_list.addVariable(variable()); })*
   v1=variable { var_list.addVariable(v1); }
		(
			COMMA v2=variable { var_list.addVariable(v2); }
		)* // { return new VariableListNode(); }
	;

variable returns[VariableNode variable]:
	vname=variable_name { return vname; }
	| vnum=variable_number { return vnum; }
	;

constraint returns[ConstraintNode constraint]:
   e=expr a=assign b=bound { return new ConstraintNode(e, a, b); }
	;

assign returns[AssignmentNode assign]:
	GT { return new AssignmentNode(AssignmentNode.AssignmentType.GreaterThan); }
	| LT { return new AssignmentNode(AssignmentNode.AssignmentType.LessThan); }
	| GEQ { return new AssignmentNode(AssignmentNode.AssignmentType.GreaterThanOrEqualTo); }
	| LEQ { return new AssignmentNode(AssignmentNode.AssignmentType.LessThanOrEqualTo); }
	| EQ { return new AssignmentNode(AssignmentNode.AssignmentType.EqualTo); }
	| NEQ { return new AssignmentNode(AssignmentNode.AssignmentType.NotEqualTo); }
	;


expr returns[ExpressionNode expr] @init { expr = new ExpressionNode(); bool neg=false; }:
	t1=term { expr.addTermNode(t1); }
		(
			(PLUS | MINUS { neg=true; }) t2=term { t2.neg = neg; expr.addTermNode(t2); }
		)* 
	;

term returns[TermNode term] @init { term = new TermNode(); bool mult=false; }:
	f1=factor {term.addFirstFactorNode(f1); }
		(
			(MULT { mult=true; } | DIV) f2=factor { if(mult) term.addFactorNode(f2, "*"); else term.addFactorNode(f2, "/"); }
		)*
	;

factor returns[FactorNode factor]:
	MINUS v1=variable_name { v1.neg = true; return new FactorNode(v1); }
	| v2=variable_name { return new FactorNode(v2); }
	| MINUS v3=variable_number { v3.neg = true; return new FactorNode(v3); }
	| v4=variable_number { return new FactorNode(v4); }
	| LPAREN e=expr RPAREN { return new FactorNode(e); }
	;

bound returns[BoundNode bound] @init { bool neg=false; }:
	(MINUS { neg=true; })? n1=variable_number { n1.neg=neg; return new BoundNode(n1); }
	| n2=string { n2.stringbound = true; return new BoundNode(n2); }
	;

variable_number returns[VariableNode variable_number]:
	num=number { return new VariableNode($num.text); };

variable_name returns[VariableNode variable_name]:
	name=VARNAME { return new VariableNode($name.text); };
   
number:
	DIGIT+ '.' DIGIT+ | '.'? DIGIT+;

DIGIT: ('0'..'9');

VARNAME:
	('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9')*;
  
NEWLINE:
	'\r'?'\n';
WS:
	(' '|'\t'|'\n'|'\r')+ { $channel=HIDDEN; };

string returns[VariableNode variable_string]:
	q=QUOTED { return new VariableNode($q.text); }
	;

QUOTED:	'"' (~'"')* '"';
