using System;
using System.IO;

namespace CPTR571A {
	public class XMLPrinterVisitor : INodeVisitor {
		private int indentAmount = 5;
		private int indentSpace = 0;

		private System.IO.TextWriter @out { get; set; }

		public XMLPrinterVisitor (System.IO.TextWriter @out) {
			this.@out = @out;
		}

		public void Visit (ClauseNode programNode) {
			PrintLine("<program>");
			indent();
			programNode.subject.Accept(this);
			unindent();
			PrintLine(":-");
			indent();
			programNode.predicate.Accept(this);
			unindent();
			PrintLine(".");
			PrintLine("</program>");
		}
		public void Visit (SubjectNode subjectNode) {
			PrintLine("<subject>");
			indent();
			subjectNode.dbinfo.Accept(this);
			unindent();
			PrintLine("</subject>");
		}
		public void Visit (PredicateNode predicateNode) {
			PrintLine("<predicate>");
			indent();
			foreach (DBInfoNode db in predicateNode.dbInfos) {
				db.Accept(this);
			}
			foreach (ConstraintNode con in predicateNode.constraintNodes) {
				con.Accept(this);
			}
			unindent();
			PrintLine("</predicate>");
		}
		public void Visit (DBInfoNode dbinfoNode) {
			PrintLine("<dbinfo>");
			indent();
			dbinfoNode.varnamenode.Accept(this);
			dbinfoNode.variableListNode.Accept(this);
			unindent();
			PrintLine("</dbinfo>");
		}
		public void Visit (BoundNode boundNode) {
			PrintLine("<bound>");
			indent();
			boundNode.varNode.Accept(this);
			unindent();
			PrintLine("</bound>");
		}
		public void Visit (ConstraintNode constraintNode) {
			PrintLine("<constraint>");
			indent();
			constraintNode.en.Accept(this);
			constraintNode.on.Accept(this);
			constraintNode.bn.Accept(this);
			unindent();
			PrintLine("</constraint>");
		}
		public void Visit (VariableListNode variableListNode) {
			PrintLine("<variableList>");
			indent();
			foreach (VariableNode v in variableListNode.variables) {
				v.Accept(this);
			}
			unindent();
			PrintLine("</variableList>");
		}
		public void Visit(ExpressionNode expessionNode) {
			PrintLine("<expression>");
			indent();
			foreach (TermNode t in expessionNode.terms) {
				t.Accept(this);
			}
			unindent();
			PrintLine("</expression>");
		}
		public void Visit(TermNode termNode) {
			if(termNode.neg) {
				PrintLine("<neg_term>");
			} else {
				PrintLine("<term>");
			}
			indent();
			int count = 0;
			foreach (FactorNode f in termNode.factors) {
				if(count > 0) {
					PrintLine(termNode.opers[count-1]);
				}
				f.Accept(this);
				count++;
			}
			unindent();
			if(termNode.neg) {
				PrintLine("</neg_term>");
			} else {
				PrintLine("</term>");
			}

		}
		public void Visit(VariableNode varNode) {
			if(varNode.neg) {
				PrintLine("<neg_variable>" + varNode.varname + "</variable>");
			} else {
				PrintLine("<variable>" + varNode.varname + "</variable>");
			}
		}
		public void Visit(AssignmentNode assignNode) {
			PrintLine("<assignmentNode>");
			indent();
			switch(assignNode.type) {
			case(AssignmentNode.AssignmentType.EqualTo):
				PrintLine("=");
				break;
			case(AssignmentNode.AssignmentType.GreaterThan):
				PrintLine(">");
				break;
			case(AssignmentNode.AssignmentType.GreaterThanOrEqualTo):
				PrintLine(">=");
				break;
			case(AssignmentNode.AssignmentType.LessThan):
				PrintLine("<");
				break;
			case(AssignmentNode.AssignmentType.LessThanOrEqualTo):
				PrintLine("<=");
				break;
			case(AssignmentNode.AssignmentType.NotEqualTo):
				PrintLine("!=");
				break;
			}
			unindent();
			PrintLine("</assignmentNode>");
		}
		public void Visit (StringNode stringNode) {
			PrintLine("<string>" + stringNode.str + "</string>");
		}
		public void Visit(FactorNode factorNode) {
			PrintLine("<factorNode>");
			indent();
			if(factorNode.e != null) factorNode.e.Accept(this);
			if(factorNode.v != null) factorNode.v.Accept(this);
			unindent();
			PrintLine("</factorNode>");
		}

		public void PrintLine (string str) {
			for (int i = 0; i < indentAmount * indentSpace; i++) {
				@out.Write(" ");
			}
			@out.WriteLine(str);
		}
		
		public void Print (string str) {
			for (int i = 0; i < indentAmount * indentSpace; i++) {
				@out.Write(" ");
			}
			@out.Write(str);
		}

		public void indent() { indentSpace++; }
		public void unindent() { indentSpace--; }

	}
}

