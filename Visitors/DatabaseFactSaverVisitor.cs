using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Web.Script.Serialization;

namespace CPTR571A {
	public class DatabaseFactSaverVisitor : INodeVisitor {

		private DatabaseManager dm;
		private TableModel tm;
		private RowModel rm;
		private bool inSubject;
		JavaScriptSerializer jss = new JavaScriptSerializer();
		//private List<string> errorMessages = new List<string>();

		public DatabaseFactSaverVisitor(DatabaseManager dm) {
			this.dm = dm;
		}

		public void Visit(ClauseNode programNode) {
			programNode.subject.Accept(this);
			programNode.predicate.Accept(this);
		}

		public void Visit(SubjectNode subjectNode) {
			inSubject = true;
			subjectNode.dbinfo.Accept(this);
			inSubject = false;
		}

		public void Visit(PredicateNode predicateNode) {
			foreach(ConstraintNode c in predicateNode.constraintNodes) {
				c.Accept(this);
			}
		}

		public void Visit(DBInfoNode dbinfoNode) {
			if(inSubject) {
				tm = dm.createTable(dbinfoNode);
				if(tm == null) {
					throw new Exception("Error Creating Table: " + dbinfoNode.varnamenode.varname);
				}
			}
			dbinfoNode.variableListNode.Accept(this);
		}

		public void Visit(VariableNode varNode) {
			ColumnModel cm = dm.createColumn(tm, varNode.varname);
			if(cm == null) {
				throw new Exception("Error Creating Column: " + varNode.varname);
			}
		}
		
		public void Visit(ConstraintNode constraintNode) {
			if(rm == null) {
				rm = dm.createRow(tm);
				if(rm == null) {
					throw new Exception("Error Creating Row: ");
				}
			}

			string dataField = jss.Serialize(constraintNode);
			ConstraintModel cm = dm.createConstraint(rm, dataField);
			if(cm == null) {
				throw new Exception("Error Creating Constraint: " + dataField);
			}

		}

		public void Visit(VariableListNode variableListNode) {
			foreach(VariableNode v in variableListNode.variables) {
				v.Accept(this);
			}
		}

		public void Visit(AssignmentNode assignNode) {
			throw new NotImplementedException();
		}
		public void Visit(BoundNode boundNode) {
			throw new NotImplementedException();
		}
		public void Visit(ExpressionNode expessionNode) {
			throw new NotImplementedException();
		}
		public void Visit(TermNode termNode) {
			throw new NotImplementedException();
		}
		public void Visit(StringNode stringNode) {
			throw new NotImplementedException();
		}
		public void Visit(FactorNode factorNode) {
			throw new NotImplementedException();
		}
	}
}

