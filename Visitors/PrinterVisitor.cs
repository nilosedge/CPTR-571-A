using System;

namespace CPTR571A {
	public class PrinterVisitor : INodeVisitor {

		System.IO.TextWriter @out { get; set; }

		public PrinterVisitor (System.IO.TextWriter @out) {
			this.@out = @out;
		}

		public void Visit (ClauseNode programNode) {
			programNode.subject.Accept(this);
			Print(" :- ");
			programNode.predicate.Accept(this);
			PrintLine(".");
		}
		public void Visit (SubjectNode subjectNode) {
			subjectNode.dbinfo.Accept(this);
		}
		public void Visit(PredicateNode predicateNode) {
			foreach(DBInfoNode db in predicateNode.dbInfos) {
				db.Accept(this);
				if(predicateNode.dbInfos.IndexOf(db) != predicateNode.dbInfos.Count - 1) {
					Print(",");
				}
			}
			if(predicateNode.dbInfos.Count > 0) Print(",");

			foreach(ConstraintNode con in predicateNode.constraintNodes) {
				con.Accept(this);
				if(predicateNode.constraintNodes.IndexOf(con) != predicateNode.constraintNodes.Count - 1) {
					Print(",");
				}
			}
		}
		public void Visit (DBInfoNode dbinfoNode) {
			dbinfoNode.varnamenode.Accept(this);
			dbinfoNode.variableListNode.Accept(this);
		}
		public void Visit (BoundNode boundNode) {
			boundNode.varNode.Accept(this);
		}
		public void Visit (ConstraintNode constraintNode) {
			constraintNode.en.Accept(this);
			constraintNode.on.Accept(this);
			constraintNode.bn.Accept(this);
		}
		public void Visit (VariableListNode variableListNode) {
			Print("(");
			foreach (VariableNode v in variableListNode.variables) {
				v.Accept(this);
				if(variableListNode.variables.IndexOf(v) != variableListNode.variables.Count - 1) {
					Print(",");
				}
			}
			Print(")");
		}
		public void Visit (ExpressionNode expessionNode) {
			int count = 0;
			foreach (TermNode t in expessionNode.terms) {
				if(t.neg) {
					Print("-");
				} else {
					if(count > 0) Print("+");
				}
				t.Accept(this);
				count++;
			}
		}

		public void Visit(TermNode termNode) {
			int count = 0;
			foreach (FactorNode f in termNode.factors) {
				if(count > 0) {
					Print(termNode.opers[count-1]);
				}
				f.Accept(this);
				count++;
			}
		}
		public void Visit(VariableNode varNode) {
			if(varNode.neg) {
				Print("-" + varNode.varname);
			} else {
				Print(varNode.varname);
			}
		}
		public void Visit(AssignmentNode assignNode) {
			switch(assignNode.type) {
			case(AssignmentNode.AssignmentType.EqualTo):
				Print("=");
				break;
			case(AssignmentNode.AssignmentType.GreaterThan):
				Print(">");
				break;
			case(AssignmentNode.AssignmentType.GreaterThanOrEqualTo):
				Print(">=");
				break;
			case(AssignmentNode.AssignmentType.LessThan):
				Print("<");
				break;
			case(AssignmentNode.AssignmentType.LessThanOrEqualTo):
				Print("<=");
				break;
			case(AssignmentNode.AssignmentType.NotEqualTo):
				Print("!=");
				break;
			}
		}
		public void Visit (StringNode stringNode) {
			Print(stringNode.str);
		}
		public void Visit(FactorNode factorNode) {
			if(factorNode.e != null) {
				Print("(");
				factorNode.e.Accept(this);
				Print(")");
			}
			if(factorNode.v != null) factorNode.v.Accept(this);
		}

		public void PrintLine (string str) {
			@out.WriteLine(str);
		}
		public void Print (string str) {
			@out.Write(str);
		}
	}
}

