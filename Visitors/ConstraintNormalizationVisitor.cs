using System;
using System.Web.Script.Serialization;
using System.Collections.Generic;

namespace CPTR571A {
	public class ConstraintNormalizationVisitor : INodeVisitor {

		JavaScriptSerializer jss = new JavaScriptSerializer();

		public List<ConstraintNode> newConstraintNodes = new List<ConstraintNode>();

		public ConstraintNormalizationVisitor() {
		}

		public void Visit(ClauseNode programNode) {
			programNode.predicate.Accept(this);
			PrinterVisitor xpr = new PrinterVisitor(Console.Out);
			programNode.Accept(xpr);
		}

		public void Visit(PredicateNode predicateNode) {
			foreach(ConstraintNode c in predicateNode.constraintNodes) {
				c.Accept(this);
			}
			predicateNode.constraintNodes = newConstraintNodes;
		}

		public void Visit(ConstraintNode constraintNode) {

			//XMLPrinterVisitor xml = new XMLPrinterVisitor(Console.Out);
			//constraintNode.Accept(xml);

			if(constraintNode.isNormalized()) {
				newConstraintNodes.Add(constraintNode);
			} else {
				if(constraintNode.on.type == AssignmentNode.AssignmentType.EqualTo && !constraintNode.bn.varNode.sb) {
					String json = jss.Serialize(constraintNode);
					ConstraintNode copy = jss.Deserialize<ConstraintNode>(json);
					constraintNode.on.type = AssignmentNode.AssignmentType.GreaterThanOrEqualTo;
					copy.on.type = AssignmentNode.AssignmentType.GreaterThanOrEqualTo;
					copy.makeNegative();
					newConstraintNodes.Add(copy);
				}
				if(constraintNode.on.type == AssignmentNode.AssignmentType.NotEqualTo) {
					String json = jss.Serialize(constraintNode);
					ConstraintNode copy = jss.Deserialize<ConstraintNode>(json);
					constraintNode.on.type = AssignmentNode.AssignmentType.GreaterThan;
					copy.on.type = AssignmentNode.AssignmentType.GreaterThan;
					copy.makeNegative();
					newConstraintNodes.Add(copy);
				}
				if(constraintNode.on.type == AssignmentNode.AssignmentType.LessThanOrEqualTo) {
					constraintNode.on.type = AssignmentNode.AssignmentType.GreaterThanOrEqualTo;
					constraintNode.makeNegative();
				}
				if(constraintNode.on.type == AssignmentNode.AssignmentType.LessThan) {
					constraintNode.on.type = AssignmentNode.AssignmentType.GreaterThan;
					constraintNode.makeNegative();
				}
				newConstraintNodes.Add(constraintNode);
			}
		}
		
		public void Visit(SubjectNode subjectNode) {
			throw new NotImplementedException();
		}
		public void Visit(DBInfoNode dbinfoNode) {
			throw new NotImplementedException();
		}
		public void Visit(BoundNode boundNode) {
			throw new NotImplementedException();
		}
		public void Visit(VariableListNode variableListNode) {
			throw new NotImplementedException();
		}
		public void Visit(ExpressionNode expessionNode) {
			throw new NotImplementedException();
		}
		public void Visit(TermNode termNode) {
			throw new NotImplementedException();
		}
		public void Visit(StringNode stringNode) {
			throw new NotImplementedException();
		}
		public void Visit(VariableNode varNode) {
			throw new NotImplementedException();
		}
		public void Visit(AssignmentNode assignNode) {
			throw new NotImplementedException();
		}
		public void Visit(FactorNode factorNode) {
			throw new NotImplementedException();
		}
	}
}

