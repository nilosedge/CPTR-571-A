using System;
using System.Data;
using Mono.Data.Sqlite;


namespace CPTR571A{
	public class SQLiteCon {

		private String connectionString;
		private IDbConnection dbcon = null;
		private IDbCommand cmd = null;
		private IDataReader reader = null;
		private String sqlfilename = null;

		public SQLiteCon() {
			sqlfilename = "Default.sqlite";
			connectionString = "URI=file:" + sqlfilename;
		}

		public void setFilename(String filename) {
			sqlfilename = filename;
			connectionString = "URI=file:" + sqlfilename;
			getConnection();
		}

		private IDbConnection getConnection() {
			if (dbcon == null) { 
				dbcon = (IDbConnection)new SqliteConnection (connectionString);
				dbcon.Open();
			}
			return dbcon;
		}

		public IDataReader createReaderWithParam(String query, String paramName, String paramValue) {
			getConnection();
			cmd = dbcon.CreateCommand();

			cmd.CommandText = query;
			cmd.CommandType = CommandType.Text;

			IDbDataParameter p = cmd.CreateParameter();
			p.ParameterName = paramName;
			p.Value = paramValue;
			cmd.Parameters.Add(p);

			//System.Console.WriteLine("Running query: " + cmd.CommandText);
			reader = cmd.ExecuteReader();
			return reader;
		}

		public IDataReader createReader(String query) {
			getConnection();
			cmd = dbcon.CreateCommand();
			cmd.CommandText = query;
			//System.Console.WriteLine("Running query: " + query);
			reader = cmd.ExecuteReader();
			return reader;
		}

		public void closeReader() {
			reader.Close();
			reader = null;
			cmd.Dispose();
			cmd = null;
		}

		public void closeConnection() {
			if(dbcon != null) {
				dbcon.Close();
				dbcon = null;
			}
		}

		public void createTables() {
			SqliteConnection.CreateFile(sqlfilename);
			getConnection();

			// Change these to the current db.

			String tableCreate = "CREATE TABLE tables (id INTEGER PRIMARY KEY, name VARCHAR(255));";
			String rowCreate = "CREATE TABLE rows (id INTEGER PRIMARY KEY, tableId INTEGER, FOREIGN KEY(tableId) REFERENCES tables(id));";
			String constraintCreate = "CREATE TABLE constraints (id INTEGER PRIMARY KEY, rowId INTEGER, dataField BLOB, FOREIGN KEY(rowId) REFERENCES rows(id));";
			String columnCreate = "CREATE TABLE columns (id INTEGER PRIMARY KEY, tableID INTEGER, name VARCHAR(255), FOREIGN KEY(tableId) REFERENCES tables(id));";

			createReader(tableCreate);
			createReader(rowCreate);
			createReader(constraintCreate);
			createReader(columnCreate);

		}
	}
}

