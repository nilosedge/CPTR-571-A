using System;

namespace CPTR571A {
	public class DatabaseCreator {

		private SQLiteCon con;

		protected TablesDAO tableDAO;
		protected RowDAO rowDAO;
		protected ConstraintDAO constraintDAO;
		protected ColumnDAO columnDAO;


		public DatabaseCreator(String filename) {
			con = new SQLiteCon();
			switchDatabase(filename);
		}

		public void switchDatabase(String filename) {
			if(con != null) {
				con.closeConnection();
			}
			if(System.IO.File.Exists(filename)) {
				openDatabase(filename);
			} else {
				createDatabase(filename);
			}
		}

		private void openDatabase(String filename) {
			con.setFilename(filename);
			setupDAOs();
		}

		private void createDatabase(String filename) {
			con.setFilename(filename);
			con.createTables();
			setupDAOs();
		}

		private void setupDAOs() {
			tableDAO = new TablesDAO(con);
			rowDAO = new RowDAO(con);
			constraintDAO = new ConstraintDAO(con);
			columnDAO = new ColumnDAO(con);
		}

		private void closeDatabase(String filename) {
			con.closeConnection();
		}

	}
}

