using System;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;

namespace CPTR571A {
	public class SQLiteDAO {

		private SQLiteCon con;

		public SQLiteDAO (SQLiteCon con) {
			this.con = con;
		}

		protected IDataReader runQuery (String query) {
			return con.createReader(query);
		}

		protected IDataReader runQueryWithParam (String query, String paramName, String paramValue) {
			return con.createReaderWithParam(query, paramName, paramValue);
		}

		protected void closeQuery() {
			con.closeReader();
		}
	}
}

