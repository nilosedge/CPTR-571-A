using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class DatabaseManager : DatabaseCreator {

		public DatabaseManager(String filename) : base(filename) {

		}

		public TableModel createTable(DBInfoNode dbinfoNode) {
			String tableName = dbinfoNode.varnamenode.varname;
			TableModel tm = tableDAO.tableExists(tableName);
			if(tm == null ) {
				tm = tableDAO.createTable(tableName); 
			}
			return tm;
		}

		public ColumnModel createColumn(TableModel tm, string varname) {
			ColumnModel cm = columnDAO.columnExists(tm.id, varname);
			if(cm == null) {
				cm = columnDAO.createColumn(tm.id, varname);
			}
			return cm;
		}

		public RowModel createRow(TableModel tm) {
			return rowDAO.createRow(tm.id);
		}

		public ConstraintModel createConstraint(RowModel rm, string serializedObject) {
			return constraintDAO.createConstraint(rm.id, serializedObject);
		}

		public TableModel getTableModelByName(string name) {
			TableModel tm = tableDAO.getTableModelByName(name);
			tm.Rows = rowDAO.getRowModelsByTableId(tm.id);
			foreach ( RowModel r in tm.Rows) {
				r.Constraints = constraintDAO.getConstraintModelsByRowID(r.id);
			}
			tm.Columns = columnDAO.getColumnModelsByTableId(tm.id);

			return tm;
		}

		public void removeTable(string name) {
			TableModel tm = tableDAO.tableExists(name);
			if(tm == null) {
				return;
			}
			foreach( RowModel r in tm.Rows) {
				constraintDAO.deleteConstraintModelByRowId(r.id);
			}
			rowDAO.deleteRowModelsByTableId(tm.id);
			columnDAO.deleteColumnModelsByTableId(tm.id);
			tableDAO.deleteTableModelByName(name);
			return;
		}

	}
}

