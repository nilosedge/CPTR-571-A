using System;
using System.Collections.Generic;
using System.Data;

namespace CPTR571A {
	public class ColumnDAO : SQLiteDAO {

		private String sqlTablename;

		public ColumnDAO(SQLiteCon con) : base(con) {
			sqlTablename = "columns";
		}

		public List<ColumnModel> getColumnModelsByTableId(long tableId) {
			List<ColumnModel> list = new List<ColumnModel>();
			IDataReader reader = runQuery("select * from " + sqlTablename + " where tableId = " + tableId);
			while(reader.Read()) {
				list.Add(populateModel(reader));
			}
			closeQuery();
			return list;
		}

		public ColumnModel getColumModelByName(string name) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where name = \"" + name +"\"");
			ColumnModel c = null;
			if(reader.Read()) {
				c = populateModel(reader);
			}
			closeQuery();
			return c;
		}

		public ColumnModel getColumnModelByTableIdName(long tableId, string name) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where tableId = " + tableId + " and name = \"" + name + "\"");
			ColumnModel c = null;
			if(reader.Read()) {
				c = populateModel(reader);
			}
			closeQuery();
			return c;
		}

		public ColumnModel columnExists(long id, string varname) {
			return getColumnModelByTableIdName(id, varname);
		}

		public ColumnModel createColumn(long tableId, string name) {
			runQuery("insert into " + sqlTablename + " (tableID, name) values(\"" + tableId + "\",\"" + name +"\")");
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = last_insert_rowid()");
			ColumnModel c = null;
			if(reader.Read()) {
				c = populateModel(reader);
			}
			closeQuery();
			return c;
		}

		public void deleteColumnModelsByTableId(long tableId) {
			runQuery("delete from " + sqlTablename + " where tableId=\"" + tableId + "\"");
		}

		private ColumnModel populateModel(IDataReader reader) {
			ColumnModel c = new ColumnModel();
			c.id = reader.GetInt64(0);
			c.tableId = reader.GetInt64(1);
			c.name = reader.GetString(2);
			return c;
		}
	}
}

