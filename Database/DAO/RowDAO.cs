using System;
using System.Data;
using System.Collections.Generic;

namespace CPTR571A {
	public class RowDAO : SQLiteDAO {
	
		private String sqlTablename;
		
		public RowDAO(SQLiteCon con) : base(con) {
			sqlTablename = "rows";
		}

		public List<RowModel> getRowModelsByTableId(long tableId) {
			List<RowModel> list = new List<RowModel>();
			IDataReader reader = runQuery("select * from " + sqlTablename + " where tableId = " + tableId);
			while(reader.Read()) {
				list.Add(populateModel(reader));
			}
			closeQuery();
			return list;
		}

		public RowModel getRowModelById(long id) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = " + id);
			RowModel r = null;
			if(reader.Read()) {
				r = populateModel(reader);
			}
			closeQuery();
			return r;
		}

		public RowModel createRow(long tableId) {
			runQuery("insert into " + sqlTablename + " (tableId) values(\"" + tableId + "\")");
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = last_insert_rowid()");
			RowModel r = null;
			if(reader.Read()) {
				r = populateModel(reader);
			}
			closeQuery();
			return r;
		}

		private RowModel populateModel(IDataReader reader) {
			RowModel r = new RowModel();
			r.id = reader.GetInt64(0);
			r.tableId = reader.GetInt64(1);
			return r;
		}

		public void deleteRowModelsByTableId(long tableId) {
			runQuery("delete from " + sqlTablename + " where tableId=\"" + tableId + "\"");
			// assumes something else takes care of the cascade delete to constraints
		}
	}
}

