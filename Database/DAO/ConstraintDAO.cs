using System;
using System.Data;
using System.Collections.Generic;

namespace CPTR571A {
	public class ConstraintDAO : SQLiteDAO {
	
		private String sqlTablename;
		
		public ConstraintDAO(SQLiteCon con) : base(con) {
			sqlTablename = "constraints";
		}

		public List<ConstraintModel> getConstraintModelsByRowID(long rowId) {
			List<ConstraintModel> list = new List<ConstraintModel>();
			IDataReader reader = runQuery("select * from " + sqlTablename + " where rowId = " + rowId);
			while(reader.Read()) {
				list.Add(populateModel(reader));
			}
			closeQuery();
			return list;
		}

		public ConstraintModel getConstraintModelById(long id) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = " + id);
			ConstraintModel c = null;
			if(reader.Read()) {
				c = populateModel(reader);
			}
			closeQuery();
			return c;
		}

		public ConstraintModel createConstraint(long rowId, string dataField) {
			runQueryWithParam("insert into " + sqlTablename + " (rowId, dataField) values(\"" + rowId + "\", :dataField)", ":dataField", dataField);
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = last_insert_rowid()");
			ConstraintModel c = null;
			if(reader.Read()) {
				c = populateModel(reader);
			}
			closeQuery();
			return c;
		}

		public void deleteConstraintModelByRowId(long rowId) {
			runQuery("delete from " + sqlTablename + " where rowId=\"" + rowId + "\"");
		}

		private ConstraintModel populateModel(IDataReader reader) {
			ConstraintModel cm = new ConstraintModel();
			cm.id = reader.GetInt64(0);
			cm.rowId = reader.GetInt64(1);
			cm.dataField = reader.GetString(2);
			return cm;
		}
	}
}

