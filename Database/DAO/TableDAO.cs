using System;
using System.Collections.Generic;
using Mono.Data.Sqlite;
using System.Data;

namespace CPTR571A {
	public class TablesDAO : SQLiteDAO {

		private String sqlTablename;

		public TablesDAO(SQLiteCon con) : base(con) {
			this.sqlTablename = "tables";
		}

		public List<TableModel> getTableModels() {
			List<TableModel> list = new List<TableModel>();
			IDataReader reader = runQuery("select * from " + sqlTablename);
			while(reader.Read()) {
				list.Add(populateModel(reader));
			}
			closeQuery();
			return list;
		}

		public TableModel getTableModelById(long id) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where id = " + id);
			TableModel t = null;
			if(reader.Read()) {
				t = populateModel(reader);
			}
			closeQuery();
			return t;
		}

		public TableModel getTableModelByName(String name) {
			IDataReader reader = runQuery("select * from " + sqlTablename + " where name = \"" + name + "\"");
			TableModel t = null;
			if(reader.Read()) {
				t = populateModel(reader);
			}
			closeQuery();
			return t;
		}

		private TableModel populateModel(IDataReader reader) {
			TableModel t = new TableModel();
			t.id = reader.GetInt64(0);
			t.name = reader.GetString(1);
			return t;
			// assumes that something else higher up will construct all the objects under this object
		}

		public TableModel tableExists(string tableName) {
			return getTableModelByName(tableName);
		}

		public TableModel createTable(string name) {
			runQuery("insert into " + sqlTablename + " (name) values(\"" + name + "\")");
			return getTableModelByName(name);
		}

		public void deleteTableModelByName(string name) {
			runQuery("delete from " + sqlTablename + " where name=\"" + name + "\"");
			// assumes something else takes care of the cascade delete to columns, rows, constraints
		}

		public void deleteTableModelById(long id) {
			runQuery("delete from " + sqlTablename + " where id=\"" + id + "\"");
			// assumes something else takes care of the cascade delete to columns, rows, constraints
		}
	}
}

