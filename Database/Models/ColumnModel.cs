using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class ColumnModel {

		public long id {get; set;}
		public long tableId {get; set;}
		public string name {get; set;}
	
		public ColumnModel() {
		}
	}
}