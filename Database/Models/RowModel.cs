using System;
using System.Collections.Generic;

namespace CPTR571A {

	public class RowModel {
		public long id { get; set; }
		public long tableId { get; set; }

		public List<ConstraintModel> Constraints = new List<ConstraintModel>();

		public RowModel() {

		}
	}
}