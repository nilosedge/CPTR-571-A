using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class TableModel {

		public long id { get; set; }
		public String name { get; set; }

		public List<ColumnModel> Columns = new List<ColumnModel>();
		public List<RowModel> Rows = new List<RowModel>();
		
		public TableModel() {

		}
	}
}

