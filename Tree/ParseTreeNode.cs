using System;

namespace CPTR571A {
	public abstract class ParseTreeNode {
		public abstract void Accept(INodeVisitor visitor);

		public ParseTreeNode () {
		}
	}
}