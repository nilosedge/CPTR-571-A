using System;

namespace CPTR571A {
	public interface INodeVisitor {

		void Visit (ClauseNode programNode);
		void Visit (SubjectNode subjectNode);
		void Visit (PredicateNode predicateNode);
		void Visit (DBInfoNode dbinfoNode);
		void Visit (BoundNode boundNode);
		void Visit (ConstraintNode constraintNode);
		void Visit (VariableListNode variableListNode);
		void Visit (ExpressionNode expessionNode);
		void Visit (TermNode termNode);
		void Visit (StringNode stringNode);
		void Visit (VariableNode varNode);
		void Visit (AssignmentNode assignNode);
		void Visit (FactorNode factorNode);

	}
}

