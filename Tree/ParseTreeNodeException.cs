
using System;
using Antlr.Runtime;

class ParseTreeNodeException : Exception {
	public string[] tokenNames { get; set; }
	public RecognitionException recognitionException { get; set; }

	public ParseTreeNodeException (string[] tokenNames, RecognitionException e) {
		this.tokenNames = tokenNames;
		this.recognitionException = e;
	}
}