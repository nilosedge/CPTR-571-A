using System;

namespace CPTR571A {
	public class AssignmentNode : ParseTreeNode {

		public AssignmentType type { get; set; }

		public AssignmentNode() {

		}

		public AssignmentNode(AssignmentNode.AssignmentType type) {
			this.type = type;
		}

		public enum AssignmentType {
			EqualTo = 1,
			GreaterThan = 2,
			GreaterThanOrEqualTo = 3,
			LessThan = 4,
			LessThanOrEqualTo = 5,
			NotEqualTo = 6,
			UNKNOWN = 7
		}

		public override void Accept(INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public bool isNormalized() {
			if(type == AssignmentType.GreaterThan || type == AssignmentType.GreaterThanOrEqualTo) {
				return true;
			} else {
				return false;
			}
		}
	}
}