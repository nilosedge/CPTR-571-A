using System;

namespace CPTR571A {
	public class ConstraintNode : ParseTreeNode {
		public ExpressionNode en { get; set; }
		public AssignmentNode on { get; set; }
		public BoundNode bn { get; set; }

		public ConstraintNode (ExpressionNode expressionNode, AssignmentNode operatorNode, BoundNode boundNode) {
			this.en = expressionNode;
			this.on = operatorNode;
			this.bn = boundNode;
		}

		public ConstraintNode () {
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public bool isNormalized() {
			return on.isNormalized();
		}

		public void makeNegative() {
			en.makeNegative();
			bn.makeNegative();
		}
	}
}

