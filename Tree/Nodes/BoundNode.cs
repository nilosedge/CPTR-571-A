using System;

namespace CPTR571A {
	public class BoundNode : ParseTreeNode {

		public VariableNode varNode { get; set; }

		public BoundNode() {
		}

		public BoundNode (VariableNode varNode) {
			this.varNode = varNode;
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public void makeNegative() {
			if(!varNode.sb) {
				varNode.neg = !varNode.neg;
			}
		}
	}
}

