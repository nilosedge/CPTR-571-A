using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class ExpressionNode : ParseTreeNode {

		public List<TermNode> terms = new List<TermNode>();

		public ExpressionNode () {
		}

		public void addTermNode(TermNode term) {
			terms.Add(term);
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public void makeNegative() {
			foreach(TermNode t in terms) {
				t.neg = !t.neg;
			}
		}
	}
}

