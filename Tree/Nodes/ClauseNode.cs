using System;

namespace CPTR571A {
	public class ClauseNode : ParseTreeNode {
		public SubjectNode subject { get; set; }
		public PredicateNode predicate { get; set; }

		public ClauseNode() {
		}

		public ClauseNode (SubjectNode subject, PredicateNode predicate) {
			this.subject = subject;
			this.predicate = predicate;
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public bool isFact() {
			return predicate.isFact();
		}
	}
}