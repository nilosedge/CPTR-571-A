using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class FactorNode : ParseTreeNode {
		public VariableNode v { get; set; }
		public ExpressionNode e { get; set; }

		public FactorNode() {
		}

		public FactorNode(VariableNode v) {
			this.v = v;
		}

		public FactorNode(ExpressionNode e) {
			this.e = e;
		}

		public override void Accept(INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}

}

