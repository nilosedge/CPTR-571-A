using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class DBInfoNode : ParseTreeNode {

		public VariableNode varnamenode { get; set; }
		public VariableListNode variableListNode { get; set; }

		public DBInfoNode() {
		}

		public DBInfoNode (VariableNode varnamenode, VariableListNode variableListNode) {
			this.varnamenode = varnamenode;
			this.variableListNode = variableListNode;
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

	}
}

