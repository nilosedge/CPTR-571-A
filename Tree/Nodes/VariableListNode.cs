using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class VariableListNode : ParseTreeNode {
		public List<VariableNode> variables = new List<VariableNode>();

		public VariableListNode () {
		}

		public void addVariable (VariableNode variableNode) {
			variables.Add(variableNode);
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}
}

