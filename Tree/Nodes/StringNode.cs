using System;

namespace CPTR571A {
	public class StringNode : ParseTreeNode {

		public string str { get; set;} 

		public StringNode () {
		}

		public StringNode (string str) {
			this.str = str;
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}
}

