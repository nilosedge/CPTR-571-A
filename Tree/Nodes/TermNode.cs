using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class TermNode : ParseTreeNode {

		public List<FactorNode> factors = new List<FactorNode>();
		public List<string> opers = new List<string>();
		public bool neg = false;

		public TermNode () {
		}

		public void addFirstFactorNode(FactorNode first) {
			factors.Add(first);
		}

		public void addFactorNode(FactorNode factor, string oper) {
			factors.Add(factor);
			opers.Add(oper);
		}
		
		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}
}

