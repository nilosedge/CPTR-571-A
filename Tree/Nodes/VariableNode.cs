using System;

namespace CPTR571A
{
	public class VariableNode : ParseTreeNode {
		public bool neg { get; set; }
		public bool sb { get; set; }
		public bool isen { get; set; }
		public string varname { get; set; }
		public ExpressionNode e { get; set; }

		public VariableNode() {
		}

		public VariableNode(string varname) {
			this.varname = varname;
		}

		public VariableNode(ExpressionNode e) {
			isen = true;
			this.e = e;
		}

		public override void Accept(INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}
}

