using System;
using System.Collections.Generic;

namespace CPTR571A {
	public class PredicateNode : ParseTreeNode {
		public List<DBInfoNode> dbInfos = new List<DBInfoNode>();
		public List<ConstraintNode> constraintNodes = new List<ConstraintNode>();

		public void addDBInfo (DBInfoNode db) {
			dbInfos.Add(db);
		}

		public void addConstraint(ConstraintNode con) {
			constraintNodes.Add(con);
		}

		public PredicateNode () {
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}

		public bool isFact() {
			if(dbInfos.Count > 0) {
				return false;
			} else return true;
		}
	}
}

