using System;

namespace CPTR571A {
	public class SubjectNode : ParseTreeNode {
		public DBInfoNode dbinfo { get; set; }

		public SubjectNode() {
		}

		public SubjectNode (DBInfoNode dbinfo) {
			this.dbinfo = dbinfo;
		}

		public override void Accept (INodeVisitor visitor) {
			visitor.Visit(this);
		}
	}
}

