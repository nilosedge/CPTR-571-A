CPTR-571-A
==========

CPTR-571-A


SQL Schema:

	Tables:
		id: int64
		name: string

	Columns:
		id: int64
		tableId: int64
		varname: string

	Rows:
		id: int64
		tableid: int64

	RowColumnValues:
		id: int64
		rowId: int64
		columnId: int64
		value: string

	RowConstraints:
		id: int64
		rowId: int64
		constraintId: int64

	Constraints:
		id: int64
		value: string
